'use strict';
/**
 * @ngdoc function
 * @name cronusApp.controller:BibliographyControlCtrl
 * @description
 * # BibliographyControlCtrl
 * Controller of the cronusApp
 */
 var cronus = angular.module('cronusApp');

cronus.controller('BibliographyUpdCtrl', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {

  //Do not show title menu
  $scope.disableMenu = true;

 	$http.get('http://localhost:8080/monuments/'+$routeParams.id+'/books/'+$routeParams.idb)
 	.success(function(book) {
        $scope.book = book;
      })
      .error(function(error) {
        $scope.err = error;
      });

     $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });

    $scope.update = function() {
      $http.put('http://localhost:8080/monuments/'+$routeParams.id+'/books/'+$routeParams.idb, $scope.book)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/bibliography");
      }); 
    };

    $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/bibliography");
    };

}]);



cronus.controller('BibliographyAddCtrl', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {

  //Create new model
  $scope.book = {"year": null,
   "author":"",
    "descr":"",
    "monuments":{"id": $routeParams.id}
  }


  $scope.update = function() {
      $http.post('http://localhost:8080/monuments/'+$routeParams.id+'/books', $scope.book)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/bibliography");
      }); 
  };

    $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/bibliography");
    };

}]);

