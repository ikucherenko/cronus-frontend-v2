'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:MonumentCtrl
 * @description
 * # LoginCtrl
 * Controller of the cronusApp
 */
var cronus = angular.module('cronusApp');

cronus.controller('LoginCtrl', ['$scope', '$rootScope', '$http', '$location', 'securityService',
function($scope, $rootScope, $http, $location, securityService) {

	$rootScope.loginPage = true;
	$scope.err;
	$scope.user = {"username": "", "password": ""};

	$scope.auth = function() {
  	securityService.login($scope.user)
        .then(function (response) {
          if (localStorage.getItem("authToken") != undefined) {
            $location.path('/main');
          };
        }, function(response) {
          	$scope.err = $rootScope.err;
        });
    };

}]);
