'use strict';


 var cronus = angular.module('cronusApp');

cronus.controller('ResHistoryUpdCtrl', ['$scope', '$http', '$routeParams', '$location', 'utilsService', function($scope, $http, $routeParams, $location, utils) {
    
    
  //Do not show title menu
  $scope.disableMenu = true;
 	
  $http.get(utils.API + '/monuments/'+$routeParams.id+'/reshistory/'+$routeParams.idr)
 	.success(function(reshistory) {
        $scope.reshistory = reshistory;
      })
      .error(function(error) {
        $scope.err = error;
      });

     $http.get(utils.API + '/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });

    $scope.update = function() {
      $http.put(utils.API +  '/monuments/'+$routeParams.id+'/reshistory/'+$routeParams.idr, $scope.reshistory)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/res_history");
      }); 
    };

    $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/res_history");
    };

}]);



cronus.controller('ResHistoryAddCtrl', ['$scope', '$http', '$routeParams', '$location', 'utilsService', function($scope, $http, $routeParams, $location, utils) {

//Do not show title menu
$scope.disableMenu = true;

  //Create new model
  $scope.reshistory = {
    "author":"",
    "years":"",
    "descr":"",
    "monuments":{"id":$routeParams.id}
  };


  $scope.update = function() {
      $http.post(utils.API + '/monuments/'+$routeParams.id+'/reshistory', $scope.reshistory)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/res_history");
      }); 
  };

  $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/res_history");
  };

}]);

