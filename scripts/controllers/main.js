'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cronusApp
 */
angular.module('cronusApp')
  .controller('MainCtrl', ['$scope','$rootScope','monumentsService','locationService','searchService',
  function($scope, $rootScope, monumentsService, locationService, searchService) {
    var _monuments = [];
    //ControllerAs
    var main = this;
    main.searchRegion;
    main.searchName = "";

    $scope.isMain = true;
    monumentsService.getMonuments()
        .then(function (monuments) {
            $scope.monuments = monuments;
        });

  }]);
