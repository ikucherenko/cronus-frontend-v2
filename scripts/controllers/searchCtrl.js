'use strict';

var cronus = angular.module('cronusApp');

cronus.controller('SearchCtrl', ['$scope','locationService','monumentsService','utilsService', function($scope, locationService,monumentsService,utils) {
    $scope.centuries = utils.CENTURIES;

    monumentsService.getMonuments()
        .then(function (monuments) {
            $scope.monuments = monuments;
            console.log(monuments);
        });

    locationService.getRegions()
        .then(function (regions) {
            $scope.searchRegion = regions[0];
            $scope.regions = regions;
        });

    $scope.disableMenu = true;

}]);
