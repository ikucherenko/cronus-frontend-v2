'use strict';

var cronus = angular.module('cronusApp');

cronus.controller('HeaderCtrl', ['$scope','$rootScope', 'locationService', 'searchService', '$routeParams', 'securityService', '$location', function($scope, $rootScope, locationService, searchService, $routeParams, securityService, $location) {

    //Is Admin
    $scope.isAdmin = securityService.isAdmin();
    //Get current id of monument
    $scope.id = $routeParams.id

    $scope.firstName = localStorage.getItem("firstName");
    $scope.lastName = localStorage.getItem("lastName");
    //Get list of all regions
    locationService.getRegions()
        .then(function (regions) {
            if($scope.main) {
                $scope.main.searchRegion = regions[0];
            }
            $scope.regions = regions;
        });


    $scope.logout = function() {
        securityService.logout();
        $location.path('/login/');
    };

}]);