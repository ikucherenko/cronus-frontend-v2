'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:ReconstructionCtrl
 * @description
 * # ReconstructionCtrl
 * Controller of the cronusApp
 */


var cronus = angular.module('cronusApp');

cronus.controller('ReconstructionCtrl', ['$scope', '$http', '$routeParams','utilsService', 'securityService', function ($scope, $http, $routeParams,utils, securityService) {
    $scope.isAdmin = securityService.isAdmin();
    $scope.isCustom = true;
    $scope.main = this;
    $scope.main.showName = true;
    $scope.main.showCentury = true;
    $scope.main.locationName = "реконструкціям";

    $scope.centuries = utils.CENTURIES;


    $http.get('http://localhost:8080/monuments/'+ $routeParams.id + '/reconstruction')
      .success(function(reconstruction) {
        $scope.reconstruction = reconstruction;
      })
      .error(function(error) {
        $scope.err = error;
      });

    //Number of selected model for update
    $scope.updated;
    //Numbers of selected models for delete
    $scope.deleted = [];

     $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });

      $scope.delete = function() {
      var conf = confirm("Ви впевнені що бажаєте видалити обрані записи з бази?");
        if (conf) {
      for (var i = 0; i < $scope.deleted.length; i++) {
        $http.delete('http://localhost:8080/monuments/' + $routeParams.id + '/reconstruction/' + $scope.deleted[i])
        .then(function() {
          window.location.reload();
        });
      };
    };

    };
  }]);
