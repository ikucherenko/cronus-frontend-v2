'use strict';
/**
 * @ngdoc function
 * @name cronusApp.controller:BibliographyControlCtrl
 * @description
 * # BibliographyControlCtrl
 * Controller of the cronusApp
 */
 var cronus = angular.module('cronusApp');

cronus.controller('ReconstructionUpdCtrl', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {
  
  //Do not show title menu
  $scope.disableMenu = true;

 	$http.get('http://localhost:8080/monuments/'+$routeParams.id+'/reconstruction/'+$routeParams.idr)
 	.success(function(reconstruction) {
        $scope.reconstruction = reconstruction;
      })
      .error(function(error) {
        $scope.err = error;
      });

     $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });

    // Carry out to utils or global constants
    var centuries = [
        {id: 8,name: 'VIII'},
        {id: 9,name: 'IX'},
        {id: 10,name: 'X'},
        {id: 11,name: 'XI'},
        {id: 12,name: 'XII'},
        {id: 13,name: 'XIII'},
        {id: 14,name: 'XIV'},
        {id: 15,name: 'XV'},
        {id: 16,name: 'XVI'},
        {id: 17,name: 'XVII'},
        {id: 18,name: 'XVIII'},
        {id: 19,name: 'XIX'},
        {id: 20,name: 'XX'},
        {id: 21,name: 'XXI'},
    ];

    $scope.centuries = centuries;

    $scope.update = function() {
      $http.put('http://localhost:8080/monuments/'+$routeParams.id+'/reconstruction/'+$routeParams.idr, $scope.reconstruction)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/reconstruction");
      }); 
    };

    $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/reconstruction");
    };

}]);

cronus.controller('ReconstructionAddCtrl', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {

  //Do not show title menu
  $scope.disableMenu = true;

  //Create new model
  $scope.reconstruction = {
    "author":"",
    "descr":"",
    "century":{"id":24},
    "monuments":{"id": $routeParams.id}
  };

    // Carry out to utils or global constants
    var centuries = [
        {id: 8,name: 'VIII'},
        {id: 9,name: 'IX'},
        {id: 10,name: 'X'},
        {id: 11,name: 'XI'},
        {id: 12,name: 'XII'},
        {id: 13,name: 'XIII'},
        {id: 14,name: 'XIV'},
        {id: 15,name: 'XV'},
        {id: 16,name: 'XVI'},
        {id: 17,name: 'XVII'},
        {id: 18,name: 'XVIII'},
        {id: 19,name: 'XIX'},
        {id: 20,name: 'XX'},
        {id: 21,name: 'XXI'},
    ];

    $scope.centuries = centuries;

  $scope.update = function() {
      $http.post('http://localhost:8080/monuments/'+$routeParams.id+'/reconstruction', $scope.reconstruction)
      .then(function(){
        $location.path("/monuments/"+$routeParams.id+"/reconstruction");
      }); 
  };

  $scope.cancel = function() {
      $location.path("/monuments/"+$routeParams.id+"/reconstruction");
  };

}]);

