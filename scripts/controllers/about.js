'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cronusApp
 */
angular.module('cronusApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
