

'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:MonumentCtrl
 * @description
 * # MonumentCtrl
 * Controller of the cronusApp
 */
var cronus = angular.module('cronusApp');

cronus.controller('MonumentCtrl', ['$scope','$sce', 'monumentService', '$routeParams','$log','$location', 'Upload', 'imageService', '$rootScope', 'securityService', 
    function ($scope, $sce, monumentService, $routeParams, $log, $location, Upload, imageService, $rootScope,securityService) {

    $scope.isAdmin = securityService.isAdmin();
    var monumentId = $routeParams.id;
    var photoDest = '/titlephoto';
    
    //Get monument model
    monumentService.getMonument(monumentId)
        .then(function (monument) {
            $scope.monuments = monument;
            $scope.sce = $sce;
        }, function (error) {
            $log.error(error);
        });

    var RefreshModel = function() {
        $scope.uploadingImage = false;
        $scope.selected = false;
        $scope.image = null;
        monumentService.getMonument(monumentId)
        .then(function (monument) {
            $scope.monuments = monument;

            var img = document.getElementById('title');
            img.src = '/images/'+monumentId+'/'+monumentId+'.jpg?id='+Math.random();

            $location.path('/monuments/'+monumentId);
        }, function (error) {
            $log.error(error);
        });
    };

      $scope.cancel = function () {
         RefreshModel();
    };

    $scope.deleteImage = function() {
        var conf = confirm('Ви впевнені що бажаєте видалити зображення до "'+ $scope.monuments.name+'"?');
        if (conf) {
        imageService.deleteImages(monumentId, photoDest, [])
        .then(function(response) {
            RefreshModel();
        })
        .error(function(err) {
            RefreshModel();
        });
        };

    };

        $scope.upload = function() {
        if ($scope.image && !$scope.image.$error) {
            var file = {
                'name': $scope.image.name,
                file: $scope.image,
            }

            imageService.upload(monumentId, photoDest, file)
                .then(function (response) {
                    RefreshModel();

                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";
            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб, та за розміром не більша ніж 1280х1024 пікселів";
        }
    };

    $scope.delete = function() {
        var conf = confirm('Ви впевнені що бажаєте видалити "'+ $scope.monuments.name+'"?');
        if (conf) {
        monumentService.deleteMonument(monumentId)
            .then(function (response) {
                $location.path('/main');
            }, function (error) {
                $log.error(error)
            });
        };
        };

}]);
