'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:DetailedDescriptionControllCtrl
 * @description
 * # DetailedDescriptionControllCtrl
 * Controller of the cronusApp
 */

var cronus = angular.module('cronusApp');

  cronus.controller('DetailedDescriptionControllCtrl', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location) {
    
    //Do not show title menu
    $scope.disableMenu = true;

    $http.get('http://localhost:8080/monuments/'+ $routeParams.id + '/description')
      .success(function(description) {
        $scope.description = description;
      })
      .error(function(error) {
        $scope.err = error;
      });

     $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });

    $scope.update = function() {
    $http.put('http://localhost:8080/monuments/'+ $routeParams.id + '/description', $scope.description)
    .then(function() {
      $location.path('/monuments/'+ $routeParams.id + '/detailed_description');
    });
	}

  $scope.cancel = function() {
    $location.path('/monuments/'+ $routeParams.id + '/detailed_description');
  };


  }]);