'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:MonumentControllCtrl
 * @description
 * # MonumentControllCtrl
 * Controller of the cronusApp
 */


var cronus = angular.module('cronusApp');

cronus.controller('MonumentEditCtrl', ['$scope', '$http', '$routeParams', '$templateCache', 'monumentService', 'locationService','utilsService','$location','imageService',
    function ($scope, $http, $routeParams, $templateCache, monumentService, locationService, utils, $location, imageService) {

    //Do not show title menu
    $scope.disableMenu = true;

    var monumentId = $routeParams.id;
    $scope.uploadingImage = false;
    $scope.centuries = utils.CENTURIES;
    console.log($routeParams);

    locationService.getCities().then(function (cities) {
        $scope.cities = cities;
        console.log(cities);
    });

    //Get monument from update
    monumentService.getMonument(monumentId)
        .then(function (monument) {
            $scope.monument = monument;
        }, function (response) {
            $scope.err = error;
        });

    //Get list of all regions
    locationService.getRegions()
        .then(function (regions) {
        $scope.regions = regions;
    });


    $scope.setRegion = function(value) {
        locationService.setCurrentRegion(value);
        //Update cities list from region
        locationService.getCities()
        .then(function (cities) {
        $scope.cities = cities;
      });
    };

    $scope.citySelect = function (selectedCity) {
        if(selectedCity) {
            $scope.monument.city = selectedCity.originalObject;
        }
    };

    $scope.save = function () {
        if ($scope.monument.name == "") {
            alert("Назва пам`ятки не може бути пустою");
        } else {
            monumentService.saveMonument($scope.monument, monumentId)
            .then(function (response) {
                $location.path('/monuments/'+monumentId);
            })
        };
    };

    $scope.close = function () {
        $location.path('/monuments/'+monumentId);
    };

  }]);



cronus.controller('MonumentAddCtrl', ['$scope', '$http', '$routeParams', '$templateCache', 'monumentService', 'locationService', 'utilsService', '$location',
    function ($scope, $http, $routeParams, $templateCache, monumentService, locationService, utils, $location) {

    //Prepare model
    $scope.monument = {
      "id": "",
      "name": "",
      "descr": " ",
      "date": "",
      "photo": "",
      "city": { "id": 29683},
      "century": {"id": 24},
      "type": {"id": 1}
    }

    //Do not show title menu
    $scope.disableMenu = true;

    $scope.centuries = utils.CENTURIES;

    locationService.getCities()
    .then(function (cities) {
        $scope.cities = cities;
    });

    //Get list of all regions
    locationService.getRegions()
        .then(function (regions) {
        $scope.regions = regions;
    });


    $scope.setRegion = function(value) {
        locationService.setCurrentRegion(value);

        //Update cities list from region
        locationService.getCities()
         .then(function (cities) {
        $scope.cities = cities;
    });
    };

    $scope.citySelect = function (selectedCity) {
        $scope.monument.city = selectedCity.originalObject;
    };

    $scope.save = function () {
        if ($scope.monument.name == "") {
            alert("Назва пам`ятки не може бути пустою");
        } else {
        monumentService.createMonument($scope.monument)
            .then(function (response) {
                $location.path('/monuments/'+response.id)
                console.log(response)
            }), function(response) {
          $rootScope.err = response;
          console.log(response);
         };
        };
    }

    $scope.close = function () {
        $location.path('/main');
    };

  }]);
