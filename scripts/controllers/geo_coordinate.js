'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:GeoCoordinateCtrl
 * @description
 * # GeoCoordinateCtrl
 * Controller of the cronusApp
 */
var cronus = angular.module('cronusApp');

cronus.controller('GeoCoordinateCtrl', ['$scope', 'geoService', '$routeParams', 'securityService', function ($scope, geoService, $routeParams, securityService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id, initialCoordinates;
    $scope.newCoordinates;

    geoService.getGeoCoordinates(id)
        .then(function (response) {

            initialCoordinates = response.coordinates;

            $scope.monument = response.monument;

            if(response.coordinates) {
                initMap(response.coordinates);
            } else {
                $scope.isEditing = true;
                editMap();
            }
    });

    var initMap = function initMap(coordinates) {
        var mapCenter = {
            lat: coordinates.breadth,
            lng: coordinates.longitude
        }

        var center = new google.maps.LatLng(coordinates.breadth, coordinates.longitude);


        var map = new google.maps.Map(document.getElementById('googleMap'), {
            center: mapCenter,
            scrollwheel: true,
            zoom: 8
        });

        var marker = new google.maps.Marker({
            position: mapCenter,
            map: map,
            title: $scope.monument.name
        });

            var coordInfoWindow = new google.maps.InfoWindow();
            coordInfoWindow.setContent(createInfoWindowContent(center, map.getZoom()));
            coordInfoWindow.setPosition(center);
            coordInfoWindow.open(map);

            map.addListener('zoom_changed', function() {
            coordInfoWindow.setContent(createInfoWindowContent(center, map.getZoom()));
            coordInfoWindow.open(map);
  });

    }



function createInfoWindowContent(latLng, zoom) {
  var scale = 1 << zoom;

  var worldCoordinate = project(latLng);

  var pixelCoordinate = new google.maps.Point(
      Math.floor(worldCoordinate.x * scale),
      Math.floor(worldCoordinate.y * scale));

  var tileCoordinate = new google.maps.Point(
      Math.floor(worldCoordinate.x * scale / TILE_SIZE),
      Math.floor(worldCoordinate.y * scale / TILE_SIZE));

  return [
    '<b>'+$scope.monument.name+'</b>',
    '<b>Довгота: </b>' + latLng.lat(),
    '<b>Широта: </b>'  + latLng.lng(),
    '<b>Масштабування: </b>' + zoom,
    '<b>Світова координата: </b>' + worldCoordinate
  ].join('<br>');
}

var TILE_SIZE = 256;

function project(latLng) {
  var siny = Math.sin(latLng.lat() * Math.PI / 180);

  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
  // about a third of a tile past the edge of the world tile.
  siny = Math.min(Math.max(siny, -0.9999), 0.9999);

  return new google.maps.Point(
      TILE_SIZE * (0.5 + latLng.lng() / 360),
      TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
}



    var editMap = function () {
        var map = new google.maps.Map(document.getElementById('googleMap'), {
            center: {lat: 49, lng: 32},
            scrollwheel: true,
            zoom: 6
        });

        var marker;

        map.addListener('click', function (mouseEvent) {
            if(marker) marker.setMap(null);

            marker = new google.maps.Marker({
                position: mouseEvent.latLng,
                map: map,
                title: $scope.monument.name
            });

            $scope.newCoordinates = {
                breadth: mouseEvent.latLng.lat(),
                longitude: mouseEvent.latLng.lng()
            }

        });

    }

    $scope.isEditing = false;

    $scope.edit = function () {
        $scope.isEditing = true;
        editMap()
    }

    $scope.save = function () {
        $scope.isEditing = false;
        initialCoordinates = $scope.newCoordinates || initialCoordinates;
        geoService.setGeoCoordinates(id,initialCoordinates);
        initMap(initialCoordinates);
    }

    $scope.cancel = function () {
        $scope.isEditing = false;
        console.log(initialCoordinates);
        initMap(initialCoordinates);
    }
}]);
