'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:DetailedDescriptionCtrl
 * @description
 * # DetailedDescriptionCtrl
 * Controller of the cronusApp
 */

var cronus = angular.module('cronusApp');

  cronus.controller('DetailedDescriptionCtrl', ['$scope', '$sce', '$http', '$routeParams', 'securityService', function ($scope, $sce, $http, $routeParams, securityService) {
    $http.get('http://localhost:8080/monuments/'+ $routeParams.id + '/description')
      .success(function(description) {
        $scope.isAdmin = securityService.isAdmin();
        $scope.description = description;
        $scope.sce = $sce;
      })
      .error(function(error) {
        $scope.err = error;
      });

     $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });
  }]);