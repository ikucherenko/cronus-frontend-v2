'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:PhotosCtrl
 * @description
 * # PhotosCtrl
 * Controller of the cronusApp
 */
var cronus = angular.module('cronusApp');

//Blueprints Controller
cronus.controller('BlueprintsCtrl', ['$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService', 'securityService',
    function ($scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService, securityService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/blueprints';
    var photoDestFront = '/blueprints';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Креслення";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані креслення?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
      };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);

//Old Photos Controller
cronus.controller('OldPhotosCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/oldphotos';
    var photoDestFront = '/old_photos';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Старі фото";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);



//New Photos Controller
cronus.controller('NewPhotosCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/newphotos';
    var photoDestFront = '/new_photos';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Нові фото";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);


//External Decoration Controller
cronus.controller('ExtDecorationPhotosCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/extdecoration';
    var photoDestFront = '/ext_decoration';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Зовнішнє оздоблення";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);


//Pirofillit Controller
cronus.controller('PirofillitCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/pirofillit';
    var photoDestFront = '/pirofillit';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Пірофілліт";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);


//Fresco Controller
cronus.controller('FrescoCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/fresco';
    var photoDestFront = '/fresco';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Фреска/Живопис";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);


//White Stone Details Controller
cronus.controller('WsDetailsCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/wsdetails';
    var photoDestFront = '/ws_details';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Білокам`яні деталі";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);


//Mosaic Controller
cronus.controller('MosaicCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/mosaics';
    var photoDestFront = '/mosaic';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Мозаїка";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);

//Floor Tiles Controller
cronus.controller('FloorTilesCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/floortiles';
    var photoDestFront = '/floor_tiles';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Плитка для підлоги";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);

//Other Controller
cronus.controller('OtherCtrl', ['securityService', '$scope', '$http', '$routeParams','Upload','imageService','utilsService', '$location', 'monumentService',
 function (securityService, $scope, $http, $routeParams, Upload, imageService, utils, $location,monumentService) {
    $scope.isAdmin = securityService.isAdmin();
    var id = $routeParams.id;
    var photoDest = '/other';
    var photoDestFront = '/other';
    $scope.deleted = [];
    $scope.deleteImage = false;
    $scope.title = "Інше";

    imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            console.log($scope.photos);
        }, function (error) {
            $scope.err = error;
        });

    monumentService.getMonument(id)
        .then(function(monument) {
            $scope.monument = monument
        });


    var resetUploadingImage = function () {
        $scope.image = null;
        $scope.description = null;
        $scope.error = null;
        $scope.uploadingImage = false;
        $scope.deleted = [];
        $scope.isEditable = false;

        imageService.getImages(id, photoDest)
        .then(function(response){
            $scope.photos = response.data;
            $location.path('/monuments/'+ id + photoDestFront);
        }, function (error) {
            $scope.err = error;
        });
    }

    $scope.toggleUploading = function (to) {
        $scope.uploadingImage = to;
    }

    $scope.delete = function() {
        var conf = confirm("Ви впевнені що бажаєте видалити обрані фото?");
        if (conf) {
        $scope.deleteImage = false;
        imageService.deleteImages(id, photoDest, $scope.deleted)
        .then(function(response) {
            resetUploadingImage();
        })
        .error(function(err) {
            resetUploadingImage();
        });
        };
    }

    $scope.cancel = function () {
        resetUploadingImage();
    }

    $scope.upload = function() {
        if ($scope.image && !$scope.image.$error && $scope.description) {

            var file = {
                'name': $scope.image.name,
                file: $scope.image,
                "description": $scope.description
            }

            imageService.upload(id, photoDest, file)
                .then(function (response) {
                    resetUploadingImage();
                });
        } else {
            $scope.error = "Будь ласка перевірте корректність усіх данних.";

            if($scope.image.$error) $scope.error += " Перевірте вашу фотографію. Вона повинна бути до 10мб";
            if(!$scope.description) $scope.error += " Опис обов'язковий!"
        }
    };

    $scope.edit = function(photoID, description) {
        imageService.editDescription(id, photoDest, photoID, description)
        .then(function(response) {
            resetUploadingImage();
        });
        
    };

}]);
