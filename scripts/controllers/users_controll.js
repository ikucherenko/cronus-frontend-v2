'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:UsersControllCtrl
 * @description
 * # UsersControllCtrl
 * Controller of the cronusApp
 */
 var cronus = angular.module('cronusApp');

  cronus.controller('UsersAddControllCtrl', ['$scope', '$http', 'utilsService', '$location', function ($scope, $http, utils, $location) {


  //Do not show title menu
  $scope.disableMenu = true;
  $scope.user =
  	{
  		"username":"",
  		"firstname":"",
  		"lastname":"",
  		"password":"",
  		"enable":true,
  		"role":{"id":2, "name":"USER"}
  	}

  $scope.roles;

  $http.get(utils.API + '/users/roles')
  .success(function(roles) {
  	$scope.roles = roles;
  	console.log(roles[0]);
  })
  .error(function(error) {
  	$scope.err = error;
  });

  $scope.setRole = function(selectedRole) {
  	$scope.user.role.id = selectedRole.id;
  }

  $scope.save = function () {
     if ($scope.user.username == "" || $scope.user.password == "") {
            alert("Логін або(та) пароль не можуть бути пустими");
        } else {

        $http.post(utils.API + '/users', $scope.user)
            .then(function () {
                $location.path('/users');
            });

        };
    }

  $scope.cancel = function() {
      $location.path("/users/");
    };

  }]);

  cronus.controller('UsersUpdControllCtrl', ['$scope', '$http', '$routeParams', '$location', 'utilsService', function($scope, $http, $routeParams, $location, utils) {
    
    
  //Do not show title menu
  $scope.disableMenu = true;

  $scope.currentRole;
  
  $http.get(utils.API + '/users/'+$routeParams.id)
  .success(function(user) {
        $scope.user = user;
      })
      .error(function(error) {
        $scope.err = error;
      });

    $scope.save = function() {
      $http.put(utils.API +  '/users/'+$routeParams.id, $scope.user)
      .then(function(){
        $location.path('/users/');
      }); 
    };

    $scope.cancel = function() {
      $location.path('/users/');
    };


  $http.get(utils.API + '/users/roles')
  .success(function(roles) {
    $scope.roles = roles;
    console.log(roles[0]);
  })
  .error(function(error) {
    $scope.err = error;
  });

}]);

