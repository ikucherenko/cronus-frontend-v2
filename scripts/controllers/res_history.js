'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:ResHistoryCtrl
 * @description
 * # ResHistoryCtrl
 * Controller of the cronusApp
 */

var cronus = angular.module('cronusApp');

cronus.controller('ResHistoryCtrl', ['$scope', '$http', '$routeParams', 'securityService', '$location', function ($scope, $http, $routeParams, securityService, $location) {
    $scope.isAdmin = securityService.isAdmin();
    $scope.isCustom = true;
    $scope.main = this;
    $scope.main.showName = true;
    $scope.main.showDate = true;
    $scope.main.locationName = "дослідженням";

    $http.get('http://localhost:8080/monuments/'+ $routeParams.id + '/reshistory')
      .success(function(reshistory) {
        $scope.reshistory = reshistory;
      })
      .error(function(error) {
        $scope.err = error;
      });

    $http.get('http://localhost:8080/monuments/' + $routeParams.id)
    .success(function(monument) {
      $scope.monument = monument;
    });


    //Number of selected model for update
    $scope.updated;
    //Numbers of selected models for delete
    $scope.deleted = [];

    $scope.delete = function() {
      var conf = confirm("Ви впевнені що бажаєте видалити обрані записи з бази?");
      if (conf) {
      for (var i = 0; i < $scope.deleted.length; i++) {
        $http.delete('http://localhost:8080/monuments/' + $routeParams.id + '/reshistory/' + $scope.deleted[i])
        .then(function() {
          window.location.reload();
        });
      };
    };
    };
  }]);
