'use strict';

/**
 * @ngdoc function
 * @name cronusApp.controller:UsersControllCtrl
 * @description
 * # UsersControllCtrl
 * Controller of the cronusApp
 */
 var cronus = angular.module('cronusApp');

  cronus.controller('UsersControllCtrl', ['$scope', '$http', 'securityService', '$location', function ($scope, $http, securityService, $location) {

  $scope.isAdmin = securityService.isAdmin();
  $scope.isCustom = true;
  $scope.main = this;
  $scope.main.showName = true;
  $scope.main.showDate = false;

  //Do not show title menu
  $scope.disableMenu = true;
  $scope.users;
  $scope.main.locationName = "користувачам";

	$http.get('http://localhost:8080/users')
 	.success(function(users) {
        $scope.users = users;
      })
      .error(function(error) {
        $scope.err = error;
      });

    //Number of selected model for update
    $scope.updated;
    //Numbers of selected models for delete
    $scope.deleted = [];

      $scope.delete = function() {
      var conf = confirm("Ви впевнені що бажаєте видалити обраних користувачів з бази?");
      if (conf) {
      for (var i = 0; i < $scope.deleted.length; i++) {
        $http.delete('http://localhost:8080/users/'+ $scope.deleted[i])
        .then(function() {
          $location.path('/users/');
        });
      };
    };
    
    };
  }]);
