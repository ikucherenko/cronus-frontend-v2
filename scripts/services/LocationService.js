(function () {
    'use strict';

 	var cronus = angular.module('cronusApp');

    cronus.factory("locationService", ['$http','utilsService', function ($http, utils) {

        var monuments;

        var currentRegion = {id:28};

        var citiesList;

        var err;

        var setCurrentRegion = function(value) {
        	currentRegion = value;
        	console.log(currentRegion);
        }


        var getCities = function() {
        	return $http.get(utils.API+'/getcities/'+currentRegion.id)
        		.then(function(response) {
        		return response.data;
        	},function(response) {
        		return response;
        	});
        }

        var getRegions = function () {
          return $http.get(utils.API+'/getallregions')
            .then(function(response) {
              return response.data;
            },function(response) {
              return response.data;
            });
        }

        var getCurrentRegion = function () {
            return currentRegion;
        }

        return {
            getRegions: getRegions,
            setCurrentRegion: setCurrentRegion,
            getCities: getCities,
            getCurrentRegion: getCurrentRegion
        }

    }]);

})();
