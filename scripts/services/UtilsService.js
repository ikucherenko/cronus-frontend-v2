(function () {
    'use strict';

    var cronus = angular.module('cronusApp');

    cronus.factory("utilsService", ['$http', function ($http) {
        var API = "http://localhost/api";

        var CENTURIES = [
            {id: '', name: ''},
            {id: 10,name: 'X'},
            {id: 11,name: 'XI'},
            {id: 12,name: 'XII'},
            {id: 13,name: 'XIII'},
            {id: 14,name: 'XIV'},
            {id: 15,name: 'XV'},
            {id: 16,name: 'XVI'},
            {id: 17,name: 'XVII'},
            {id: 18,name: 'XVIII'},
            {id: 19,name: 'XIX'},
            {id: 20,name: 'XX'},
            {id: 21,name: 'XXI'},
        ];


        var getCitiesByID = function(id) {
            $http.get(API+'/getcities/'+id)
            .then(function(response) {
                return response.data;
            }, function(response) {
                console.error(response);
            });
        }

        
        return {
            API:API,
            CENTURIES: CENTURIES,
            getCitiesByID: getCitiesByID
        }

    }]);
})()
