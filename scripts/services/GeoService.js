(function () {
    'use strict';

    var cronus = angular.module('cronusApp');

    cronus.factory("geoService", ['$http','utilsService', function ($http, utils) {

        var getGeoCoordinates = function (id) {
            return $http.get('http://localhost:8080/monuments/'+ id + '/geocoordinate')
                .then(function (response) {
                    var responseWrapepr = {};
                    console.log(response.data);
                    responseWrapepr.coordinates = {
                        breadth: parseFloat(response.data.breadth),
                        longitude: parseFloat(response.data.longitude),
                    }

                    if (!responseWrapepr.coordinates.longitude&&!responseWrapepr.coordinates.breadth) {
                        responseWrapepr.coordinates = null;
                    }

                    responseWrapepr.monument = response.data.monuments;
                    console.log(response);
                    return responseWrapepr;

                    return responseWrapepr;
                }, function (response) {
                    console.error(response);
                });
        }

        var setGeoCoordinates = function (idMonument, data) {
            var newCoordinate = {
                breadth: data.breadth,
                longitude: data.longitude,
                monuments: {id: idMonument}
            }
            
            return $http.put('http://localhost:8080/monuments/'+ idMonument + '/geocoordinate', newCoordinate)
                .then(function (response) {
                    return response;
                }, function (response) {
                    console.error(response);
                });
        }

        return {
            getGeoCoordinates: getGeoCoordinates,
            setGeoCoordinates: setGeoCoordinates
        }

    }]);

})();
