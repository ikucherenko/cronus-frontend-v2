(function () {
    'use strict';

    var cronus = angular.module('cronusApp');

    cronus.factory("monumentsService", ['$http','utilsService', function ($http, utils) {
        var monuments;

        var getMonuments = function () {
            return $http.get(utils.API+'/monuments')
                .then(function(response) {
                    return response.data;
                },function(response) {
                    return response.data;
                });
        }

        return {
            getMonuments: getMonuments
        }

    }]);

})();