(function () {
    'use strict';

    var cronus = angular.module('cronusApp');

    cronus.factory("imageService", ['$http','utilsService','Upload', function ($http, utils,Upload) {

        var PHOTO_DESTINATIONS = {
            blueprints: '/blueprints',
            oldphotos: '/oldphotos',
            newphotos: '/newphotos',
            extdecoration: '/extdecoration',
            pirofillit: '/pirofillit',
            fresco: '/fresco',
            wsdetails: '/wsdetails',
            floortiles: '/floortiles'
        };

        var uploadImage = function (id, photoDest, file) {
            return Upload.upload({
                url: utils.API + '/monuments/' + id + photoDest,
                data: file
            }).then(function (response) {
                return response;
            }, function (response) {
                console.error(response);
                return response;
            })
        };

        var getImages = function (id, photoDest) {
            return $http.get(utils.API + '/monuments/'+ id + photoDest)
                .success(function(response) {
                    return response;
                })
                .error(function(error) {
                    return error;
                });
        };


        var deleteImages = function(id, photoDest, photoSet) {
               return $http({ url: utils.API + '/monuments/'+ id + photoDest, 
                method: 'DELETE', 
                data: photoSet, 
                headers: {"Content-Type": "application/json;charset=utf-8"}
             }).then(function(response) {
                    return response;
                }, function(error) {
                    return error;
                });
            };

        var editDescription = function(id, photoDest, idPhoto, description) {
                return $http.put(utils.API + '/monuments/'+ id + photoDest +'/'+ idPhoto, description)
                .success(function(response) {
                    return response;
                }, function(error) {
                    return error;
                });
        };

        return {
            upload: uploadImage,
            getImages: getImages,
            deleteImages: deleteImages,
            editDescription: editDescription
        }


    }]);

})();
