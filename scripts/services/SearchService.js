(function () {
    'use strict';

 	var cronus = angular.module('cronusApp');

    cronus.factory("searchService", ['$http','utilsService', function ($http, utils) {

        var region = "",
            name = "";

        var setRegion = function (regionName) {
            region = regionName;
        }

        var getRegion = function () {
            return region;
        }

        var setName = function (searchName) {
            name = searchName;
        }

        var getName = function () {
            return name;
        }

        return {
            setRegion: setRegion,
            getRegion: getRegion,
            setName: setName,
            getName: getName
        }

    }]);

})();
