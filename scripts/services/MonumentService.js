(function () {
    'use strict';

    var cronus = angular.module('cronusApp');

    cronus.factory("monumentService", ['$http','utilsService', function ($http, utils) {
        
        var monument;

        var getMonument = function (id) {
            return $http.get(utils.API+'/monuments/'+id)
                .then(function(response) {
                    return response.data;
                },function(response) {
                    return response;
                });
        }

        var deleteMonument = function (id) {
            return $http.delete(utils.API+'/monuments/'+id)
                .then(function(response) {
                    return  response;
                }, function (response) {
                    return response;
                });
        }

        var saveMonument = function (monument, id) {
            return $http.put(utils.API+'/monuments/'+id, monument)
                .then(function (response) {
                    return response;
                }, function (response) {
                    console.error(response);
                });
        }


        var createMonument = function (monument) {
            return $http.post(utils.API+'/monuments', monument)
                .then(function(response) {
                    var id = response.headers('Location');
                    response.id = id;
                    return response;
                }, function (response) {

                })
        }

        return {
            getMonument: getMonument,
            deleteMonument: deleteMonument,
            saveMonument: saveMonument,
            createMonument: createMonument
        }

    }]);

})();
