(function () {
  'use strict';

  var cronus = angular.module('cronusApp');

  cronus.factory("securityService", ['utilsService','$rootScope','$http', function (utils, $rootScope, $http) {

    $rootScope.authData;
    $rootScope.err;


    var login = function(user) {
  
      return $http.post(utils.API+ '/login', user)
        .then(function (response) {
          setAuthData(response.data.token);
          console.log(response.data.token)
        }, function(response) {
          $rootScope.err = response;
          $rootScope.authData = undefined;
        });
    };

    var logout = function() {
      console.log("logout")
      $rootScope.authData = undefined;
      localStorage.clear();
    };

    var isAdmin = function() {
      return localStorage.getItem("authPermission");
    };

  	var setAuthData = function(authData) {
      //Save token into local storage
     localStorage.setItem("authToken", authData);
     //Set all authorization data
      // localStorage.setItem("authToken", authData[0]);
      // localStorage.setItem("firstName", authData[1]);
      // localStorage.setItem("lastName", authData[2]);
      // localStorage.setItem("authPermission", authData[3]);

      //Set token in header for all requests
      $http.defaults.headers.common.Authorization = 'JWT ' + localStorage.getItem("authToken");
      
      $http.get(utils.API+ '/current').then(function(response){
      //Save user data to storage
      localStorage.setItem("authPermission", response.data.is_staff);
      localStorage.setItem("firstName", response.data.first_name);
      localStorage.setItem("lastName", response.data.last_name);
     });
  		$rootScope.$broadcast('authChanged');
  	};


  	var isAuthenticated = function() {
      return $rootScope.access;
    };

    return {
      login: login,
      isAdmin: isAdmin,
      setAuthData: setAuthData,
      isAuthenticated: isAuthenticated,
      logout: logout
    };
	}]);
})()
