'use strict';

/**
 * @ngdoc overview
 * @name cronusApp
 * @description
 * # cronusApp
 *
 * Main module of the application.
 */
angular.module('cronusApp', [
    'textAngular',
    'checklist-model',
    'angucomplete-alt',
    '720kb.datepicker',
    'ngWYSIWYG',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngFileUpload'
  ])
  .config(function ($routeProvider) {

    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
        .when('/monuments/:id/r_history_add', {
        templateUrl: 'views/res_history_control.html',
        controller: 'ResHistoryAddCtrl',
        controllerAs: 'resHistoryAddControl'
      })
        .when('/monuments/:id/r_history_update/:idr', {
        templateUrl: 'views/res_history_control.html',
        controller: 'ResHistoryUpdCtrl',
        controllerAs: 'resHistoryUpdControl'
      })
        .when('/monuments/:id/reconstruction_add', {
        templateUrl: 'views/reconstruction_control.html',
        controller: 'ReconstructionAddCtrl',
        controllerAs: 'reconstructionAddControl'
      })
        .when('/monuments/:id/reconstruction_update/:idr', {
        templateUrl: 'views/reconstruction_control.html',
        controller: 'ReconstructionUpdCtrl',
        controllerAs: 'reconstructionUpdControl'
      })
        .when('/monuments/:id/bibliography_add', {
        templateUrl: 'views/bibliography_control.html',
        controller: 'BibliographyAddCtrl',
        controllerAs: 'bibliographyAddControl'
      })
        .when('/monuments/:id/bibliography_update/:idb', {
        templateUrl: 'views/bibliography_control.html',
        controller: 'BibliographyUpdCtrl',
        controllerAs: 'bibliographyUpdControl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/monuments/monument_add', {
        templateUrl: 'views/monument_controll.html',
        controller: 'MonumentAddCtrl',
        controllerAs: 'monumentAddControll'
      })
      .when('/monuments/:id', {
        templateUrl: 'views/monument.html',
        controller: 'MonumentCtrl',
        controllerAs: 'monument'
      })
      .when('/monuments/:id/bibliography', {
        templateUrl: 'views/bibliography.html',
        controller: 'BibliographyCtrl',
        controllerAs: 'bibliography'
      })
      .when('/monuments/:id/geo_coordinate', {
        templateUrl: 'views/geo_coordinate.html',
        controller: 'GeoCoordinateCtrl',
        controllerAs: 'geoCoordinate'
      })
      .when('/monuments/:id/other', {
        templateUrl: 'views/photos.html',
        controller: 'OtherCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/blueprints', {
        templateUrl: 'views/photos.html',
        controller: 'BlueprintsCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/old_photos', {
        templateUrl: 'views/photos.html',
        controller: 'OldPhotosCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/new_photos', {
        templateUrl: 'views/photos.html',
        controller: 'NewPhotosCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/ext_decoration', {
        templateUrl: 'views/photos.html',
        controller: 'ExtDecorationPhotosCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/fresco', {
        templateUrl: 'views/photos.html',
        controller: 'FrescoCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/pirofillit', {
        templateUrl: 'views/photos.html',
        controller: 'PirofillitCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/ws_details', {
        templateUrl: 'views/photos.html',
        controller: 'WsDetailsCtrl',
        controllerAs: 'photos'
      })
       .when('/monuments/:id/mosaic', {
        templateUrl: 'views/photos.html',
        controller: 'MosaicCtrl',
        controllerAs: 'photos'
      })
       .when('/monuments/:id/floor_tiles', {
        templateUrl: 'views/photos.html',
        controller: 'FloorTilesCtrl',
        controllerAs: 'photos'
      })
      .when('/monuments/:id/reconstruction', {
        templateUrl: 'views/reconstruction.html',
        controller: 'ReconstructionCtrl',
        controllerAs: 'reconstruction'
      })
      .when('/monuments/:id/res_history', {
        templateUrl: 'views/res_history.html',
        controller: 'ResHistoryCtrl',
        controllerAs: 'resHistory'
      })
      .when('/users_add', {
        templateUrl: 'views/users_controll.html',
        controller: 'UsersAddControllCtrl',
        controllerAs: 'usersAddControll'
      })
      .when('/users_update/:id', {
        templateUrl: 'views/users_controll.html',
        controller: 'UsersUpdControllCtrl',
        controllerAs: 'usersUpdControll'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersControllCtrl',
        controllerAs: 'usersControll'
      })
      .when('/monuments/:id/monument_controll', {
        templateUrl: 'views/monument_controll.html',
        controller: 'MonumentEditCtrl',
        controllerAs: 'monumentControll'
      })
      .when('/monuments/:id/detailed_description', {
        templateUrl: 'views/detailed_description.html',
        controller: 'DetailedDescriptionCtrl',
        controllerAs: 'detailedDescription'
      })
      .when('/monuments/:id/detailed_description_controll', {
        templateUrl: 'views/detailed_description_controll.html',
        controller: 'DetailedDescriptionControllCtrl',
        controllerAs: 'detailedDescriptionControll'
      })
      .when('/search', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl',
      })
      .otherwise({
        redirectTo: '/'
      });

/*      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });*/
  })
  .run(function($rootScope, $location, $http) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
     if (localStorage.getItem("authToken") == null) {
      $location.path("/login");
     } else {
        $http.defaults.headers.common.Authorization = 'JWT ' + localStorage.getItem("authToken");
     }
    });
  })
