(function () {

    angular.module('cronusApp')
        .directive('header', function() {
            return {
                templateUrl: '/views/header.html'
            };
        });

})();
