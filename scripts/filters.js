angular.module('cronusApp')
    .filter('region', function() {
        return function(input, searchRegion) {
            var output = [];
            if(!searchRegion) return input;
            if(input){
                for (var i = 0; i < input.length; i++) {
                    var regionId = input[i].city.region.id;

                    //Here we doesn't filter monuments if user selected region - 'Усі області'
                    if(regionId == searchRegion.id || searchRegion.id == 28 ){
                        output.push(input[i]);
                    }

                }
            }
            return output;
        };
    })
